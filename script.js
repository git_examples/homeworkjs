function buttonClick() {
    var x1 = document.getElementById("x1").value;
    var x2 = document.getElementById("x2").value;
    resultDiv = document.getElementById("result");
    if(x1.trim() === "" || x2.trim() === "") {
        alert("Поля x1 и x2 должны быть заполнены.")
    } 
    else {
        x1 = parseInt(x1);
        x2 = parseInt(x2);
        if(isNaN(x1)||isNaN(x2)){
            alert("В полях х1 и х2 должны быть введены числовые значения.")
        }
        else if(x1 >= x2){
            alert("x1 должен быть больше, чем х2")
        }
        else {
            var sum = document.getElementById("sum");
            var product = document.getElementById("product");
            if(sum.checked) {
                var count = 0;
                for(var i = x1; i <= x2; i++) {
                    count += i;
                }
                resultDiv.innerHTML = "";
                resultDiv.append("Сумма всех чисел от х1 до х2: " + count);
            }
            else if(product.checked) {
                var count = 1;
                for(var i = x1; i <= x2; i++) {
                    count *= i;
                }
                resultDiv.innerHTML = "";
                resultDiv.append("Произведение всех чисел от х1 до х2: " + count);
            }
            else {
                var arr = [];
                cont : for(var i = x1; i <= x2; i++) {
                    for(var j = 2; j < i; j++) {
                        if(i % j === 0) continue cont;
                    }
                    arr.push(i);
                }
                resultDiv.innerHTML = "";
                resultDiv.append("Все простые числа от х1 до х2: " + arr.join(", "));
            }
        }
    }
}

function buttonClean(){
    var x1 = document.getElementById("x1");
    var x2 = document.getElementById("x2");
    x1.value = "";
    x2.value = "";
}